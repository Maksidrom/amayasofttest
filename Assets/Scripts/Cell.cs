﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour, iCell
{
    [SerializeField]
    private Sprite[] LetterSprites;

    private Transform _transform;
    private GameObject sendLevelResult;
    private SpriteRenderer spriteRenderer;

    private int letterSprite;
    private bool isLetterForFind;

    public int FindLetter { get; set; } = '1';

    public bool IsLetterForFind
    {
        get { return isLetterForFind; }
        set { isLetterForFind = value; }
    }


    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
        sendLevelResult = GameObject.Find("Level");
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            spriteRenderer.sprite = LetterSprites[FindLetter];
        }
        
        
        StartCoroutine(BounceEffect(false));
    }

    public IEnumerator BounceEffect(bool sendResultOnWin)
    {
        float bounceSpeed = 3;
        transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        while (transform.localScale.x < 1.2f)
        {
            transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * bounceSpeed, transform.localScale.y + Time.deltaTime * bounceSpeed, transform.localScale.z + Time.deltaTime * bounceSpeed);
            yield return null;
        }

        while (transform.localScale.x > 0.8f)
        {
            transform.localScale = new Vector3(transform.localScale.x - Time.deltaTime * bounceSpeed, transform.localScale.y - Time.deltaTime * bounceSpeed, transform.localScale.z - Time.deltaTime * bounceSpeed);
            yield return null;
        }

        while (transform.localScale.x < 1f)
        {
            transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * bounceSpeed, transform.localScale.y + Time.deltaTime * bounceSpeed, transform.localScale.z + Time.deltaTime * bounceSpeed);
            yield return null;
        }

        if (sendResultOnWin)
        {
            sendLevelResult.GetComponent<Level>().CheckResult(FindLetter);
        }
    }

    public IEnumerator EaseOutBounce()
    {
        const float n1 = 7.5625f;
        const float d1 = 2.75f;
        float x = 0f; //Прогресс анимации(движение)       
        var startPositionX = transform.position;

        while (true)
        {
            if (x < 1 / d1)
            {
                float moving = n1 * x * x;
                transform.position = new Vector3(transform.position.x + Time.deltaTime, transform.position.y, transform.position.z);
                x += Time.deltaTime;
                yield return null;
            }
            else if (x < 2 / d1)
            {
                float moving = n1 * (x - 1.5f / d1) * x + 0.75f;
                transform.position = new Vector3(transform.position.x - Time.deltaTime, transform.position.y, transform.position.z);
                x += Time.deltaTime;
                yield return null;
            }
            else if (x < 2.5 / d1)
            {
                float moving = n1 * (x - 2.25f / d1) * x + 0.9375f;
                transform.position = new Vector3(transform.position.x + Time.deltaTime, transform.position.y, transform.position.z);                
                x += Time.deltaTime;
                yield return null;
            }
            else
            {
                Debug.Log("End coroutine");
                transform.position = startPositionX;
                sendLevelResult.GetComponent<Level>().CheckResult(FindLetter);
                yield break;
            }
        }
    }

    private IEnumerator LoseEaseInBounceEffect()
    {
        
        yield return null;
    }

    public void OnMouseDown()
    {
        if (!isLetterForFind)
        {
            StartCoroutine(EaseOutBounce());
        }
        else
        {
            StartCoroutine(BounceEffect(true));
            //sendLevelResult.GetComponent<Level>().CheckResult(FindLetter);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
