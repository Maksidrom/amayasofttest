using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceEffects : MonoBehaviour
{
    public void BounceEffect()
    {
        StartCoroutine(BounceEffectExecuter());
    }

    public void BounceEffect(Transform objecTransform, bool checkOnWin)
    {
        BounceEffectCoroutineEnumeratorOnWin(checkOnWin);
    }

    public void EaseOutBounce()
    {
        
    }
    
    private IEnumerator BounceEffectExecuter()
    {
        float bounceSpeed = 3;
        transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        while (transform.localScale.x < 1.2f)
        {
            transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * bounceSpeed, transform.localScale.y + Time.deltaTime * bounceSpeed, transform.localScale.z + Time.deltaTime * bounceSpeed);
            yield return null;
        }

        while (transform.localScale.x > 0.8f)
        {
            transform.localScale = new Vector3(transform.localScale.x - Time.deltaTime * bounceSpeed, transform.localScale.y - Time.deltaTime * bounceSpeed, transform.localScale.z - Time.deltaTime * bounceSpeed);
            yield return null;
        }

        while (transform.localScale.x < 1f)
        {
            transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * bounceSpeed, transform.localScale.y + Time.deltaTime * bounceSpeed, transform.localScale.z + Time.deltaTime * bounceSpeed);
            yield return null;
        }

        yield return true;
    }

    private IEnumerator BounceEffectCoroutinEnumerator()
    {
        float bounceSpeed = 3;
        transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        while (transform.localScale.x < 1.2f)
        {
            transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * bounceSpeed, transform.localScale.y + Time.deltaTime * bounceSpeed, transform.localScale.z + Time.deltaTime * bounceSpeed);
            yield return null;
        }

        while (transform.localScale.x > 0.8f)
        {
            transform.localScale = new Vector3(transform.localScale.x - Time.deltaTime * bounceSpeed, transform.localScale.y - Time.deltaTime * bounceSpeed, transform.localScale.z - Time.deltaTime * bounceSpeed);
            yield return null;
        }

        while (transform.localScale.x < 1f)
        {
            transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * bounceSpeed, transform.localScale.y + Time.deltaTime * bounceSpeed, transform.localScale.z + Time.deltaTime * bounceSpeed);
            yield return null;
        }
    }

    private IEnumerator BounceEffectCoroutineEnumeratorOnWin(bool checkOn)
    {
        yield return null;
    }

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(BounceEffectExecuter());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
