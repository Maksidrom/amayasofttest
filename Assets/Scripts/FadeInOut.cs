using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour
{
    [SerializeField] private CanvasGroup myUiGroup;
    [SerializeField] private bool fadeIn = false;
    [SerializeField] private bool fadeOut = false;

    public void Show()
    {
        fadeIn = false;
        StartCoroutine(ShowUI());
    }

    public void Hide()
    {
        fadeOut = false;
    }

    private IEnumerator ShowUI()
    {
        while (myUiGroup.alpha < 0.8f)
        {
            myUiGroup.alpha += Time.deltaTime;
            yield return null;    
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (fadeIn)
        {
            if (myUiGroup.alpha < 1)
            {
                myUiGroup.alpha += Time.deltaTime;
                if (myUiGroup.alpha >= 1)
                {
                    fadeIn = false;
                }
            }
        }

        if (fadeOut)
        {
            if (myUiGroup.alpha >= 0)
            {
                myUiGroup.alpha -= Time.deltaTime;
                if (myUiGroup.alpha == 0)
                {
                    fadeOut = false;
                }
            }
        }
    }
}
