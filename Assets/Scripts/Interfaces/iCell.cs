using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iCell
{
    IEnumerator BounceEffect(bool sendResultOnWin);
    IEnumerator EaseOutBounce();
    void OnMouseDown();
}
