﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    [SerializeField] private Button btnRestartGame;
    [SerializeField] private GameObject imgFade;
    [SerializeField] private Transform Spawn;
    
    private int _level;
    private int letterForFind;
    
    // Start is called before the first frame update
    void Start()
    {
        _level = 1;
        letterForFind = Spawn.GetComponent<Spawn>().SpawnLevel(_level);
        btnRestartGame.gameObject.SetActive(false);
    }

    public void CheckResult(int cellAnswer)
    {
        if (cellAnswer == letterForFind)
        {
            Debug.Log("True");
            if (_level < 3)
            {
                _level++;
                Spawn.GetComponent<Spawn>().DeleteInstantiateGrid();
                letterForFind = Spawn.GetComponent<Spawn>().SpawnLevel(_level);
            }
            else
            {
                Debug.Log(_level);
                imgFade.GetComponent<FadeInOut>().Show();
                Spawn.GetComponent<Spawn>().DisableCellsCollider();
                btnRestartGame.gameObject.SetActive(true);
            }
        }
        else
        {
            Debug.Log("False");
            Spawn.GetComponent<Spawn>().DeleteInstantiateGrid();
            letterForFind = Spawn.GetComponent<Spawn>().SpawnLevel(_level);
        }
    }

    public void ResetScene()
    {
        SceneManager.LoadScene("SampleScene");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
