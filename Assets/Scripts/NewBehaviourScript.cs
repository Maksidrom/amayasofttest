using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public Sprite[] LettersSprites1;

    private SpriteRenderer SpriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (SpriteRenderer != null)
            {
                foreach (var VARIABLE in LettersSprites1)
                {
                    Debug.Log(VARIABLE.name.Last());
                }
                //SpriteRenderer.sprite = A;
            }
            //GetComponent<SpriteRenderer>().sprite = A;          
        }
    }
}
