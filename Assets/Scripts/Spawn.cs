﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;
using UnityEngine.UI;

public class Spawn : MonoBehaviour
{
    [SerializeField]
    private Transform cellPrefab;
    [SerializeField]
    private Text textLetterForFind;

    private int rangeForInstantiateCell;
    private float startInstantiateCellX;
    private float startInstantiateCellY;

    //private char LetterForFind;

    private List<int> _listLetters;
    private List<Transform> _gameObjects;
    public Spawn(int level)
    {
        SpawnLevel(level);
    }
    

    public int SpawnLevel(int level)
    {
        _listLetters = new List<int>();
        _gameObjects = new List<Transform>();
        rangeForInstantiateCell = 4;
        

        for (var i = 0; i < level * 3; ++i)
        {
            while (true)
            {
                int createNewLetterForCell = Random.Range(0, 25);
                if (!_listLetters.Contains(createNewLetterForCell))
                {
                    _listLetters.Add(createNewLetterForCell);
                    break;
                }
            }
        }
        
        //определение буквы для поиска
        int letterForFind = _listLetters[Random.Range(0, level * 3)];
        InstantiateGrid(level, letterForFind);
        
        string alphabet = "abcdefghijklmnopqrstuvwxyz";
        textLetterForFind.text = "Find letter " + alphabet[letterForFind].ToString().ToUpper();
        StartCoroutine(FadeOutCr());
        return letterForFind;
    }

    private void InstantiateGrid(int level, int letterForFind)
    {
        if (level == 1)
        {
            startInstantiateCellX = -4;
            foreach (var letter in _listLetters)
            {
                Transform cell = Instantiate(cellPrefab, new Vector3(startInstantiateCellX, 0, 0), Quaternion.identity);
                _gameObjects.Add(cell);
                cell.GetComponent<Cell>().FindLetter = letter;
                if (letter == letterForFind)
                {
                    cell.GetComponent<Cell>().IsLetterForFind = true;
                }

                startInstantiateCellX += 4;
            }
        }
        else if(level == 2)
        {
            int i = 0;
            if (i < 3)
            {
                startInstantiateCellX = -4;
                startInstantiateCellY = 2;
                for (i = 0; i < 3; i++)
                {
                    Transform cell = Instantiate(cellPrefab, new Vector3(startInstantiateCellX, startInstantiateCellY, 0), Quaternion.identity);
                    _gameObjects.Add(cell);
                    cell.GetComponent<Cell>().FindLetter = _listLetters[i];
                    if (_listLetters[i] == letterForFind)
                    {
                        cell.GetComponent<Cell>().IsLetterForFind = true;
                    }

                    startInstantiateCellX += 4;
                }
            }

            if (i >= 3)
            {
                startInstantiateCellX = -4;
                startInstantiateCellY = -1.5f;
                for (i = 3;i < level * 3; i++)
                {
                    Transform cell = Instantiate(cellPrefab, new Vector3(startInstantiateCellX, startInstantiateCellY, 0), Quaternion.identity);
                    _gameObjects.Add(cell);
                    cell.GetComponent<Cell>().FindLetter = _listLetters[i];
                    if (_listLetters[i] == letterForFind)
                    {
                        cell.GetComponent<Cell>().IsLetterForFind = true;
                    }
                    startInstantiateCellX += 4;
                }
            }
        }
        else
        {
            int i = 0;
            if (i < 3)
            {
                startInstantiateCellX = -4;
                startInstantiateCellY = 3.9f;
                for (i = 0; i < 3; i++)
                {
                    Transform cell = Instantiate(cellPrefab, new Vector3(startInstantiateCellX, startInstantiateCellY, 0), Quaternion.identity);
                    _gameObjects.Add(cell);
                    cell.GetComponent<Cell>().FindLetter = _listLetters[i];
                    if (_listLetters[i] == letterForFind)
                    {
                        cell.GetComponent<Cell>().IsLetterForFind = true;
                    }
                    startInstantiateCellX += 4;
                }
            }

            if (i < 6)
            {
                startInstantiateCellX = -4;
                startInstantiateCellY = 0.7f;
                for (i = 3;i < 6; i++)
                {
                    Transform cell = Instantiate(cellPrefab, new Vector3(startInstantiateCellX, startInstantiateCellY, 0), Quaternion.identity);
                    _gameObjects.Add(cell);
                    cell.GetComponent<Cell>().FindLetter = _listLetters[i];
                    if (_listLetters[i] == letterForFind)
                    {
                        cell.GetComponent<Cell>().IsLetterForFind = true;
                    }
                    startInstantiateCellX += 4;
                }
            }

            if (i >= 6)
            {
                float startInstantiateCellX = -4;
                float startInstantiateCellY = -2.5f;
                for (i = 6;i < 9; i++)
                {
                    Transform cell = Instantiate(cellPrefab, new Vector3(startInstantiateCellX, startInstantiateCellY, 0), Quaternion.identity);
                    _gameObjects.Add(cell);
                    cell.GetComponent<Cell>().FindLetter = _listLetters[i];
                    if (_listLetters[i] == letterForFind)
                    {
                        cell.GetComponent<Cell>().IsLetterForFind = true;
                    }
                    startInstantiateCellX += 4;
                }
            }
        }
    }

    public void DisableCellsCollider()
    {
        foreach (var cell in _gameObjects)
        {
            cell.GetComponent<Collider2D>().enabled = false;
        }
    }

    public void DeleteInstantiateGrid()
    {
        foreach (var gameObject in _gameObjects)
        {
            Destroy(gameObject.gameObject);
        }
    }

    private IEnumerator FadeOutCr()
    {
        textLetterForFind.color = new Color(textLetterForFind.color.r, textLetterForFind.color.g, textLetterForFind.color.b, 0);
        while (textLetterForFind.color.a < 1.0f)
        {
            textLetterForFind.color = new Color(textLetterForFind.color.r, textLetterForFind.color.g, 
                textLetterForFind.color.b, textLetterForFind.color.a + (Time.deltaTime / 2f));
            yield return null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
